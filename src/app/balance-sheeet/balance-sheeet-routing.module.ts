import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'assets'
  },
  {
    path: '',
    component: MainNavComponent,
    children: [
      {
        path: 'assets',
        loadChildren: () =>
          import('./assets/assets.module').then(
            (assets) => assets.AssetsModule
          ),
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BalanceSheeetRoutingModule { }
