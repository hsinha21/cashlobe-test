import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BalanceSheeetRoutingModule } from './balance-sheeet-routing.module';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [MainNavComponent, SideNavComponent],
  imports: [
    CommonModule,
    BalanceSheeetRoutingModule,
    SharedModule
  ],
  exports: [SharedModule]
})
export class BalanceSheeetModule { }
