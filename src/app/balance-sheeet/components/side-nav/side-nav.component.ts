import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  // isHandset$: Observable<boolean> = this.breakpointObserver
  //   .observe(Breakpoints.Handset)
  //   .pipe(
  //     map((result) => result.matches),
  //     shareReplay()
  //   );
  showFiller = true;

  menu = [
    {
      id: 'dashboard',
      path: 'assets',
      title: 'Assets',
      icon: 'dashboard',
    },
    {
      id: 'dashboard',
      path: 'assets',
      title: 'Liabilties ',
      icon: 'dashboard',
    }
  ];

  constructor(private breakpointObserver: BreakpointObserver,) { }

  ngOnInit(): void {
  }

}
