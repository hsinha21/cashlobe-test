import { Location } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { XeroService } from 'src/app/xero.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit {

  tenantID = ''

  allTenents: any = []

  isTenentSelcted: Boolean = false;
  isReportReady: Boolean = false;
  isAuth: Boolean = false;

  report: any;

  balanceSheetData: any;

  periodOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

  constructor(
    private xeroService: XeroService,
    private location: Location
  ) { }

  ngOnInit(): void {

    console.log(history.state.data);

    if (history.state.data === undefined) {
      this.location.back();
      return
    }


    this.getTenents();
    // this.report = this.xeroService.getBalanceSheetData()
    // this.setTableData();
  }

  getTenents() {
    this.xeroService.getXeroTenents(history.state.data._id).subscribe(
      (res: any) => {
        console.log(res);
        if (res.data.Status) {
          console.log(res);
          // this.isAuth = true
        }
        else {
          this.allTenents = res.data;
          // this.isAuth = false;
        }

      }, error => {
        console.log(error);

      }
    );
  }

  tenentReport(value) {
    console.log(value);
    if (value == undefined) {
      this.isTenentSelcted = true;
      return false;
    } else {
      this.isReportReady = false;
      this.isTenentSelcted = false;
      this.tenantID = value;
      let data = {
        tenantID: this.tenantID,
        userId: history.state.data._id
      }
      this.xeroService.getbalanceSheetData(data).subscribe(
        (res: any) => {
          console.log(res);
          // this.isReportReady = true;
          this.isReportReady = true;
          this.report = res.data

          this.setTableData();
        }, error => {
          console.log(error);

        }
      );
    }
  }

  tenentReportFilter(timeFrame, period) {
    let data = {
      tenantID: this.tenantID,
      timeFrame: timeFrame,
      period: period,
      userId: history.state.data._id

    }
    this.xeroService.getbalanceSheetData(data).subscribe(
      (res: any) => {
        console.log(res);
        this.isReportReady = true;
        this.report = res.data

        this.setTableData();
        // this.xeroService.setBalanceSheetData(res.data)
        // this.report = this.xeroService.getBalanceSheetData()
      }, error => {
        console.log(error);

      }
    );
  }

  setTableData() {
    let data: any = {
      dates: this.report.Reports[0].Rows[0].Cells,
      finalData: {}
    }

    let final = []

    for (let i = 0; i < this.report.Reports[0].Rows.length; i++) {

      if (this.report.Reports[0].Rows[i].RowType === "Section") {
        let iData: any = {
          title: '',
          data: []
        }
        iData.title = this.report.Reports[0].Rows[i].Title
        for (let j = 0; j < this.report.Reports[0].Rows[i].Rows.length; j++) {
          iData.data.push(this.report.Reports[0].Rows[i].Rows[j].Cells)
        }
        final.push(iData)
      }
    }

    data.finalData = final;
    this.balanceSheetData = data
  }

}
