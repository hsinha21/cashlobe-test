import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssetsRoutingModule } from './assets-routing.module';
import { CurrentAssetsComponent } from './components/current-assets/current-assets.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [CurrentAssetsComponent],
  imports: [
    CommonModule,
    AssetsRoutingModule, SharedModule
  ]
})
export class AssetsModule { }
