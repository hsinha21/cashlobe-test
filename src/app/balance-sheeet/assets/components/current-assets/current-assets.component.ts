import { Component, OnInit } from '@angular/core';
import { XeroService } from 'src/app/xero.service';

@Component({
  selector: 'app-current-assets',
  templateUrl: './current-assets.component.html',
  styleUrls: ['./current-assets.component.scss']
})
export class CurrentAssetsComponent implements OnInit {

  report: any;
  bankData: any = [];

  constructor(private xeroService: XeroService) { }

  ngOnInit(): void {
    this.report = this.xeroService.getBalanceSheetData()
    console.log(this.report);
    this.setTableData();
  }

  setTableData() {
    let data: any = {
      dates: this.report.Reports[0].Rows[0].Cells,
      bankDetails: [],
      currentAssetData: [],
      fixedAssetData: [],
      finalData: {}
    }

    for (let i = 0; i < this.report.Reports[0].Rows.length; i++) {
      if (this.report.Reports[0].Rows[i].Title === "Bank") {
        for (let j = 0; j < this.report.Reports[0].Rows[i].Rows.length; j++) {
          data.bankDetails.push(this.report.Reports[0].Rows[i].Rows[j].Cells)
        }
      }

      if (this.report.Reports[0].Rows[i].Title === "Current Assets") {
        for (let j = 0; j < this.report.Reports[0].Rows[i].Rows.length; j++) {
          data.currentAssetData.push(this.report.Reports[0].Rows[i].Rows[j].Cells)
        }
      }

      if (this.report.Reports[0].Rows[i].Title === "Fixed Assets") {
        for (let j = 0; j < this.report.Reports[0].Rows[i].Rows.length; j++) {
          data.fixedAssetData.push(this.report.Reports[0].Rows[i].Rows[j].Cells)
        }
      }
    }

    let final = []

    for (let i = 0; i < this.report.Reports[0].Rows.length; i++) {

      if (this.report.Reports[0].Rows[i].RowType === "Section") {
        let iData: any = {
          title: '',
          data: []
        }
        iData.title = this.report.Reports[0].Rows[i].Title
        for (let j = 0; j < this.report.Reports[0].Rows[i].Rows.length; j++) {
          iData.data.push(this.report.Reports[0].Rows[i].Rows[j].Cells)
        }
        final.push(iData)
      }

      // if (this.report.Reports[0].Rows[i].Title === "Bank") {
      //   for (let j = 0; j < this.report.Reports[0].Rows[i].Rows.length; j++) {
      //     data.bankDetails.push(this.report.Reports[0].Rows[i].Rows[j].Cells)
      //   }
      // }

      // if (this.report.Reports[0].Rows[i].Title === "Current Assets") {
      //   for (let j = 0; j < this.report.Reports[0].Rows[i].Rows.length; j++) {
      //     data.currentAssetData.push(this.report.Reports[0].Rows[i].Rows[j].Cells)
      //   }
      // }

      // if (this.report.Reports[0].Rows[i].Title === "Fixed Assets") {
      //   for (let j = 0; j < this.report.Reports[0].Rows[i].Rows.length; j++) {
      //     data.fixedAssetData.push(this.report.Reports[0].Rows[i].Rows[j].Cells)
      //   }
      // }
    }

    data.finalData = final

    console.log(final);


    console.log(data);
    this.bankData = data
  }

}
