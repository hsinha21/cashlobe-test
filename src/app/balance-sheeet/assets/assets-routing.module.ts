import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CurrentAssetsComponent } from './components/current-assets/current-assets.component';

const routes: Routes = [
  { path: '', component: CurrentAssetsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetsRoutingModule { }
