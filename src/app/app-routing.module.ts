import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'balance-sheet',
    loadChildren: () => import('./balance-sheeet/balance-sheeet.module').then((bSheet) => bSheet.BalanceSheeetModule),
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then((admin) => admin.AdminModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
