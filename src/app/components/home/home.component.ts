import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { XeroService } from 'src/app/xero.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isTokenAdded: Boolean = false;

  constructor(private route: ActivatedRoute, private xero: XeroService, private router: Router) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(query => {
      console.log(query);
      if (query.code) {
        this.xero.signInWithXero(query.code).subscribe((res: any) => {
          console.log(res);

          let data = {
            xeroUserData: JSON.parse(atob(res.id_token.split('.')[1])),
            accessToken: res.access_token,
            refreshToken: res.refresh_token
          }
          this.xero.addXeroDataOfUser(data).subscribe(
            (res: any) => {
              console.log(res);
              this.isTokenAdded = true;
            }
          );

          // this.getTenents(res)
          // localStorage.setItem('xero_access_token', res.access_token);
          // localStorage.setItem('xero_refresh_token', res.refresh_token);
          // localStorage.setItem('xero_id_token', res.id_token);

          // this.router.navigate(['/balance-sheet'])
          // this.refresToken(res)
        }, error => {
          console.log(error);

        })

      }
    })

    // this.route.paramMap.subscribe((params: any) => {
    //   console.log(params.params);

    // this.id = params.params.id;
    //Do some logic with the id
    // })
  }

  refresToken(res) {
    this.xero.getRefreshToken(res).subscribe((res) => {
      console.log(res);

    }, error => {
      console.log(error);

    })
  }



}
