import { Component } from '@angular/core';
import { XeroService } from './xero.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cashlobe-ui';

  constructor(private xeroService: XeroService) { }

  signIn() {
    // this.xeroService.signInWithXero()
  }
}
