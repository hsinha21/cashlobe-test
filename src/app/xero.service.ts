import { HttpClient, HttpHeaders } from '@angular/common/http';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class XeroService {

  // url = `https://ec2-18-223-1-40.us-east-2.compute.amazonaws.com:4200/apis`
  url = `http://localhost:4000/apis`


  balanceSheetData: any;

  constructor(
    private http: HttpClient
  ) { }

  addNewUser(data) {
    return this.http.post(this.url + `/addNewUser`, data);
  }

  getAllUsers() {
    return this.http.get(this.url + `/getAllUsers`);
  }

  sendConsentEmail(data) {
    return this.http.post(this.url + `/sendConsentEmail`, data);
  }

  addXeroDataOfUser(data) {
    return this.http.post(this.url + `/addXeroDataOfUser`, data);
  }

  setBalanceSheetData(data) {
    this.balanceSheetData = data
  }

  getBalanceSheetData() {
    return this.balanceSheetData;
  }

  signInWithXero(code) {
    let head = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': `Basic ${btoa('8090A29A30E14C6DA18F23BE33770316' + ":" + 'SAMCNN6EwdoYaWuutqTZsYR7rk7twyq8uZGAcvEU2jtRjIxJ')}`
    }
    let body = `grant_type=authorization_code&code=${code}&redirect_uri=http://localhost:4200`

    // console.log(btoa('8090A29A30E14C6DA18F23BE33770316' + ":" + 'SAMCNN6EwdoYaWuutqTZsYR7rk7twyq8uZGAcvEU2jtRjIxJ'));

    return this.http.post(`https://identity.xero.com/connect/token`, body, { headers: head })
  }

  getbalanceSheetData(data) {
    return this.http.post(this.url + `/getUserBalanceSheetReport`, data);
  }

  getXeroTenents(userId) {
    return this.http.get(this.url + `/getUserTenents/${userId}`)
    // return this.http.get(this.url + `/xeroTenantConnections/${localStorage.getItem('xero_refresh_token')}`)
  }

  getRefreshToken(res) {
    const httpOptions = {
      headers: new HttpHeaders({
        'grant_type': `refresh_token`,
        'Content-Type': 'application/json'
      })
    }
    let head = {
      'grant_type': `refresh_token`,
      'Content-Type': 'application/json'
    }
    let data = {
      grant_type: "refresh_token",
      refresh_token: res.refresh_token,
      client_id: '8090A29A30E14C6DA18F23BE33770316',
      client_secret: 'SAMCNN6EwdoYaWuutqTZsYR7rk7twyq8uZGAcvEU2jtRjIxJ'
    }
    console.log(data);

    let body = `grant_type=refresh_token&refresh_token=${res.refresh_token}&client_id=8090A29A30E14C6DA18F23BE33770316&client_secret=SAMCNN6EwdoYaWuutqTZsYR7rk7twyq8uZGAcvEU2jtRjIxJ`

    let formData: FormData = new FormData();
    formData.append('grant_type', 'refresh_token');
    formData.append('refresh_token', res.refresh_token);
    formData.append('client_id', '8090A29A30E14C6DA18F23BE33770316');
    formData.append('client_secret', 'SAMCNN6EwdoYaWuutqTZsYR7rk7twyq8uZGAcvEU2jtRjIxJ');

    return this.http.post(`https://identity.xero.com/connect/token`, formData, { headers: head })
  }
}
