import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddUsersComponent } from './components/add-users/add-users.component';
import { ViewUsersComponent } from './components/view-users/view-users.component';

const routes: Routes = [
  { path: '', redirectTo: 'view-users', pathMatch: 'full' },
  { path: 'view-users', component: ViewUsersComponent },
  { path: 'add-user', component: AddUsersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

