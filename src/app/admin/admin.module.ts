import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { LoginComponent } from './components/login/login.component';
import { ViewUsersComponent } from './components/view-users/view-users.component';
import { AddUsersComponent } from './components/add-users/add-users.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [LoginComponent, ViewUsersComponent, AddUsersComponent],
  imports: [
    CommonModule,
    AdminRoutingModule, SharedModule
  ]
})
export class AdminModule { }
