import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { XeroService } from 'src/app/xero.service';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.scss']
})
export class ViewUsersComponent implements OnInit {

  allUsers: any = [];

  constructor(
    private xeroService: XeroService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.fetchAllUsers();
  }

  fetchAllUsers() {
    this.xeroService.getAllUsers().subscribe(
      (res: any) => {
        console.log(res);
        this.allUsers = res.data;
      }
    );
  }

  viewUserReport(item) {
    console.log(item);

    this.router.navigate(['./balance-sheet'], { state: { data: item } })
  }

  sendConsentEmail(item) {
    // console.log(item);
    this.xeroService.sendConsentEmail(item).subscribe(
      (res: any) => {
        console.log(res);
        if (res.success) {
          item.sent = 'Sent'
        }
      }
    );
  }

}
