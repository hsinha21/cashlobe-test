import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { XeroService } from 'src/app/xero.service';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.scss']
})
export class AddUsersComponent implements OnInit {

  constructor(
    public location: Location,
    private xeroService: XeroService
  ) { }

  ngOnInit(): void {
  }

  addUserSubmit(name, email, org, role) {
    console.log(name, email, org, role);

    let data = {
      name: name,
      email: email,
      org: org,
      role: role
    }

    this.xeroService.addNewUser(data).subscribe(
      (res: any) => {
        console.log(res);
        if (res.success) {
          this.location.back();
        }
      }
    );
  }

}
